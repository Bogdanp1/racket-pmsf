#lang info


(define pkg-desc "PMS formatting and parsing functions. Common library.")

(define version "3.1.1")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "threading-lib"))

(define build-deps
  '())
