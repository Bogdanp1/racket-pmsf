;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang typed/racket/base

(require/typed/provide pmsf/name
  ;; Special
  [pversion-string?     (-> Any Boolean)]
  [pcategory?           (-> Any Boolean)]
  [pname?               (-> Any Boolean)]
  ;; Struct
  [#:struct pversion
   ([release            : String]
    [alpha              : (Listof (Option String))]
    [beta               : (Listof (Option String))]
    [pre                : (Listof (Option String))]
    [rc                 : (Listof (Option String))]
    [p                  : (Listof (Option String))]
    [revision           : Exact-Nonnegative-Integer])
   #:type-name PVersion]
  [#:struct pfull
   ([name               : String]
    [version            : (Option PVersion)])
   #:type-name PFull]
  [#:struct pcomplete
   ([category           : String]
    [full               : PFull])
   #:type-name PComplete]
  ;; Convert
  [pversion->string     (-> PVersion String)]
  [string->pversion     (-> String PVersion)]
  [pfull->string        (-> PFull String)]
  [string->pfull        (-> String PFull)]
  [pcomplete->string    (-> PComplete String)]
  [string->pcomplete    (-> String PComplete)])

(provide PComplete
         PFull
         PVersion)
