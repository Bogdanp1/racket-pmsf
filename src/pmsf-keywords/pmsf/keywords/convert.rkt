;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base

(require (only-in racket/string string-join string-split)
         "struct.rkt")

(provide (all-defined-out))


(define (pkeyword-functionality->string sym)
  (case sym
    [(stable)
     ""]
    [(unavailable)
     "-"]
    [(unstable)
     "~"]
    [else
     (error 'pkeyword-functionality->string
            "unknown functionality, given ~v"
            sym)]))

(define (pkeyword->string a-pkeyword)
  (string-append
   (pkeyword-functionality->string (pkeyword-functionality a-pkeyword))
   (pkeyword-architecture a-pkeyword)
   (let ([platform
          (pkeyword-platform a-pkeyword)])
     (cond
       [platform
        (string-append "-" platform)]
       [else
        ""]))))

(define (pkeywords->string a-pkeywords)
  (string-join (map pkeyword->string
                    (pkeywords-keywords a-pkeywords))
               " "))

(define (string->pkeyword a-string)
  (let* ([functionality
          (case (string-ref a-string 0)
            [(#\-)
             'unavailable]
            [(#\~)
             'unstable]
            [else
             'stable])]
         [clean-string
          (cond
            [(equal? functionality 'stable)
             a-string]
            [else
             (substring a-string 1)])]
         [splitted
          (regexp-split "-" clean-string)])
    (pkeyword functionality
              (car splitted)
              (let ([splitted-rest
                     (cdr splitted)])
                (cond
                  [(null? splitted-rest)
                   #false]
                  [else
                   (car splitted-rest)])))))

(define (string->pkeywords a-string)
  (pkeywords (map string->pkeyword
                  (string-split a-string))))
