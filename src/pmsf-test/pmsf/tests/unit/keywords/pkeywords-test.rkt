;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           pmsf/keywords)

  (test-equal? "to pkeywords: empty string"
               (string->pkeywords "")
               (pkeywords '()))

  (test-equal? "to string: empty pkeywords"
               (pkeywords->string (pkeywords '()))
               "")

  (test-equal? "to pkeywords: amd64-linux"
               (string->pkeywords "amd64-linux")
               (pkeywords (list (pkeyword 'stable "amd64" "linux"))))

  (test-equal? "to pkeywords: -amd64-linux"
               (string->pkeywords "-amd64-linux")
               (pkeywords (list (pkeyword 'unavailable "amd64" "linux"))))

  (test-equal? "to pkeywords: ~amd64-linux"
               (string->pkeywords "~amd64-linux")
               (pkeywords (list (pkeyword 'unstable "amd64" "linux"))))

  (test-equal? "pkeywords <-> string: amd64-linux"
               (pkeywords->string (string->pkeywords "amd64-linux"))
               "amd64-linux")

  (test-equal? "to pkeywords: amd64 ~arm64 -*"
               (string->pkeywords "amd64 ~arm64 -*")
               (pkeywords (list (pkeyword 'stable "amd64" #false)
                                (pkeyword 'unstable "arm64" #false)
                                (pkeyword 'unavailable "*" #false))))

  (test-equal? "pkeywords <-> string: amd64 ~arm64 -*"
               (pkeywords->string (string->pkeywords "amd64 ~arm64 -*"))
               "amd64 ~arm64 -*"))
