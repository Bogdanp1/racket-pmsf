;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang racket/base


(module+ test
  (require rackunit
           pmsf/name)

  (test-equal? "to pfull: asd-9999"
               (string->pfull "asd-9999")
               (pfull "asd" (pversion "9999" '() '() '() '() '() 0)))

  (test-equal? "to pfull: asd"
               (string->pfull "asd")
               (pfull "asd" #false))

  (test-equal? "to pfull: 2048-9999"
               (string->pfull "2048-9999")
               (pfull "2048" (pversion "9999" '() '() '() '() '() 0)))

  (test-equal? "to pfull: 2048"
               (string->pfull "2048")
               (pfull "2048" #false))

  (test-equal? "to pfull: 3s-9999"
               (string->pfull "3s-9999")
               (pfull "3s" (pversion "9999" '() '() '() '() '() 0)))

  (test-equal? "to pfull: 3s"
               (string->pfull "3s")
               (pfull "3s" #false))

  (let ([str
         "asd-1.2.3_alpha_p20220101_p1-r1"])
    (test-equal? (format "pfull <-> string: ~a" str)
                 (pfull->string (string->pfull str))
                 str)))
