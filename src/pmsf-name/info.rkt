#lang info


(define pkg-desc "PMS formatting and parsing functions. Name component.")

(define version "3.1.1")

(define pkg-authors '(xgqt))

(define license 'GPL-2.0-or-later)

(define collection 'multi)

(define deps
  '("base"
    "brag-lib"
    "reprovide-lang-lib"
    "threading-lib"
    "pmsf-lib"))

(define build-deps
  '())
