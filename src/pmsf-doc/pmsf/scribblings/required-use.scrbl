;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/condition
                     pmsf/required-use)
          scribble/example
          pmsf/required-use)


@(define example-evaluator
   (make-base-eval '(require pmsf/condition pmsf/required-use)))


@title[#:tag "pmsf-required-use"]{REQUIRED_USE}

@defmodule[pmsf/required-use]


@section[#:tag "pmsf-required-use-struct"]{REQUIRED_USE struct}

@defmodule[pmsf/required-use/struct]

@defstruct[
 prequired-use
 ([conditions (listof (or/c pcondition? string?))])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (prequired-use (list (pcondition "test" (list "debug"))))
 ]
}


@section[#:tag "pmsf-required-use-convert"]{REQUIRED_USE conversion}

@defmodule[pmsf/required-use/convert]

@defproc[
 (prequired-use->string [input-prequired-use prequired-use?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (prequired-use->string
  (prequired-use (list (pcondition "test" (list "debug")))))
 ]
}

@defproc[
 (port->prequired-use [input-port input-port?])
 prequired-use?
 ]{

}

@defproc[
 (string->prequired-use [input-string string?])
 prequired-use?
 ]{

 @examples[
 #:eval example-evaluator
 (string->prequired-use "test? ( debug )")
 ]
}
