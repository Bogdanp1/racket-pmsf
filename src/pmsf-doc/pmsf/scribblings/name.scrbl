;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     pmsf/name)
          scribble/example
          pmsf/name)


@(define example-evaluator
   (make-base-eval '(require pmsf/name)))


@title[#:tag "pmsf-name"]{Name}

@defmodule[pmsf/name]


@section[#:tag "pmsf-name-special"]{Name special elements}

@defproc[
 (pcategory? [v any])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pcategory? "app-misc")
 ]
}

@defproc[
 (pname? [v any])
 boolean?
 ]{

 @examples[
 #:eval example-evaluator
 (pcategory? "editor-wrapper")
 ]
}


@section[#:tag "pmsf-name-struct"]{Name structs}

"Name" structs mimic PMS variables derived from package/ebuild names,
for reference see
@link["https://projects.gentoo.org/pms/8/pms.html#x1-10900011.1"
 "Defined Variables (Package Manager Specification, p. 11.1)"].

@defstruct[
 pversion
 ([release  string?]
  [alpha    (listof (or/c string? #true))]
  [beta     (listof (or/c string? #true))]
  [pre      (listof (or/c string? #true))]
  [rc       (listof (or/c string? #true))]
  [p        (listof (or/c string? #true))]
  [revision exact-nonnegative-integer?])
 #:transparent
 ]{
 @racket[pversion] represents PMS's @racket{PVR}
 (package version, and revision).

 @examples[
 #:eval example-evaluator
 (pversion "1.2.3" '("4") '("5") '("6") '("7") '("8") 9)
 ]
}

@defstruct[
 pfull
 ([name    pname?]
  [version (or/c #false pversion?)])
 #:transparent
 ]{
 @racket[pfull] represents PMS's @racket{PF}
 (package name, version, and revision).

 @examples[
 #:eval example-evaluator
 (pfull "editor-wrapper"
        (pversion "1.2.3" '("4") '("5") '("6") '("7") '("8") 9))
 ]
}

@defstruct[
 pcomplete
 ([category pcategory?]
  [full     pfull?])
 #:transparent
 ]{
 @racket[pcomplete] represents PMS's @racket{CATEGORY/PF}
 (package category, name, version, and revision).

 @examples[
 #:eval example-evaluator
 (pcomplete "app-misc"
            (pfull "editor-wrapper"
                   (pversion "1.2.3" '("4") '("5") '("6") '("7") '("8") 9)))
 ]
}


@section[#:tag "pmsf-name-convert"]{Name conversion}

@subsection[#:tag "pmsf-name-convert-pversion"]{PVersion conversion}

@defproc[
 (pversion->string [input-pversion pversion?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (pversion->string (pversion "1.2.3" '("4") '("5") '("6") '("7") '("8") 9))
 ]
}

@defproc[
 (port->pversion [input-port input-port?])
 pversion?
 ]{

}

@defproc[
 (string->pversion [input-string string?])
 pversion?
 ]{

 @examples[
 #:eval example-evaluator
 (string->pversion "1.2.3_alpha4_beta5_pre6_rc7_p8-r9")
 ]
}

@subsection[#:tag "pmsf-name-convert-pfull"]{PFull conversion}

@defproc[
 (pfull->string [input-pfull pfull?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (pfull->string
  (pfull "editor-wrapper"
         (pversion "1.2.3" '("4") '("5") '("6") '("7") '("8") 9)))
 ]
}

@defproc[
 (string->pfull [input-string string?])
 pfull?
 ]{

 @examples[
 #:eval example-evaluator
 (string->pfull "editor-wrapper-1.2.3_alpha4_beta5_pre6_rc7_p8-r9")
 ]
}

@subsection[#:tag "pmsf-name-convert-pcomplete"]{PComplete conversion}

@defproc[
 (pcomplete->string [input-pcomplete pcomplete?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (pcomplete->string
  (pcomplete "app-misc"
             (pfull "editor-wrapper"
                    (pversion "1.2.3" '("4") '("5") '("6") '("7") '("8") 9))))
 ]
}

@defproc[
 (string->pcomplete [input-string string?])
 pcomplete?
 ]{

 @examples[
 #:eval example-evaluator
 (string->pcomplete
  "app-misc/editor-wrapper-1.2.3_alpha4_beta5_pre6_rc7_p8-r9")
 ]
}
