;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual


@(require ziptie/git/hash
          pmsf/version)


@(define (in-upstream path)
   (format "https://gitlab.com/gentoo-racket/racket-pmsf/-/tree/~a/" path))


@title[#:tag "pmsf"]{PMSF}

@author[@author+email["Maciej Barć" "xgqt@riseup.net"]]


@link["https://projects.gentoo.org/pms/8/pms.html"
 "Package Manager Specification"]
formatting and parsing functions for
@link["https://racket-lang.org/"
 "Racket"].

Version: @link[@in-upstream[@VERSION]]{@VERSION},
commit hash:
@(let ([git-hash (git-get-hash #:short? #true)])
   (case git-hash
     [("N/A")
      (displayln "[WARNING] Not inside a git repository!")
      git-hash]
     [else
      (link (in-upstream git-hash) git-hash)]))


@table-of-contents[]

;; Must be first
@include-section{condition.scrbl}
@include-section{name.scrbl}

;; Sort alphabetically
@include-section{depend.scrbl}
@include-section{iuse.scrbl}
@include-section{keywords.scrbl}
@include-section{required-use.scrbl}
@include-section{restrict.scrbl}
@include-section{slot.scrbl}
@include-section{src-uri.scrbl}

;; Then, not strictly PMS:
@include-section{manifest.scrbl}

;; Must be last
@include-section{typed.scrbl}
@include-section{version.scrbl}

@index-section[]
