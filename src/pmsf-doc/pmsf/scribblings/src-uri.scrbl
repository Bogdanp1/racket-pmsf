;; This file is part of racket-pmsf - PMS formatting and parsing functions.
;; Copyright (c) 2022-2023, Maciej Barć <xgqt@riseup.net>
;; Licensed under the GNU GPL v2 License
;; SPDX-License-Identifier: GPL-2.0-or-later

;; racket-pmsf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 2 of the License, or
;; (at your option) any later version.

;; racket-pmsf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with racket-pmsf.  If not, see <https://www.gnu.org/licenses/>.


#lang scribble/manual

@(require (for-label racket
                     net/url
                     net/url-structs
                     pmsf/condition
                     pmsf/src-uri)
          scribble/example
          pmsf/src-uri)


@(define example-evaluator
   (make-base-eval '(require net/url pmsf/condition pmsf/src-uri)))


@title[#:tag "pmsf-src-uri"]{SRC_URI}

@defmodule[pmsf/src-uri]


@section[#:tag "pmsf-src-uri-struct"]{SRC_URI structs}

@defmodule[pmsf/src-uri/struct]

@defstruct[
 psource
 ([uri       url?]
  [file-name (or/c #false string?)])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (psource (string->url "https://asd.asd/asd-0.tar") #false)
 ]
}

@defstruct[
 psrc-uri
 ([sources (listof (or/c pcondition? psource?))])
 #:transparent
 ]{

 @examples[
 #:eval example-evaluator
 (psrc-uri (list (psource (string->url "https://asd.asd/asd-0.tar") #false)))
 ]
}


@section[#:tag "pmsf-src-uri-convert"]{SRC_URI conversion}

@defmodule[pmsf/src-uri/convert]

@defproc[
 (psrc-uri->string [input-psrc-uri psrc-uri?])
 string?
 ]{

 @examples[
 #:eval example-evaluator
 (psrc-uri->string
  (psrc-uri (list (psource (string->url "https://asd.asd/asd-0.tar") #false))))
 (psrc-uri->string
  (psrc-uri
   (list (pcondition "amd64"
                     (list (psource (string->url "https://asd.asd/asd-0_x86.tar")
                                    "asd-0.tar"))))))
 ]
}

@defproc[
 (port->psrc-uri [input-port input-port?])
 psrc-uri?
 ]{

}

@defproc[
 (string->psrc-uri [input-string string?])
 psrc-uri?
 ]{

 @examples[
 #:eval example-evaluator
 (string->psrc-uri "https://asd.asd/asd-0.tar")
 (string->psrc-uri "amd64? ( https://asd.asd/asd-0_x86.tar -> asd-0.tar )")
 ]
}


@section[#:tag "pmsf-src-uri-query"]{SRC_URI querying}

@defmodule[pmsf/src-uri/query]

@defproc[
 (psrc-uri->sources [input-psrc-uri psrc-uri?])
 (listof psource?)
 ]{
 Extract contents of given @racket[psrc-uri],
 all @racketid[condition]s are thrown away.

 @examples[
 #:eval example-evaluator
 (psrc-uri->sources
  (string->psrc-uri
   (string-append "amd64? ( https://asd.asd/asd-0_x86.tar ) "
                  "arm64? ( https://asd.asd/asd-0_arm64.tar )")))
 ]
}
